#ifndef _RECORD_H_
#define _RECORD_H_

#include "config.h"
#include "mylibs/lnls.h"
#include "/home/jon/Code/libs/date/date.h"


struct Record
{
	size_t id;
	char type;
	Date date;
	char category[MAX_STR_CD];
	char description[MAX_STR_CD];
	float amount;
};
typedef struct Record Record;

Record rec_blank();

void rec_print_header();
void rec_print_row(Record *r, const int row_num);
char *rec_to_string(Record r);
void rec_list_print(lnls_node *list);

Record rec_input();

#endif
