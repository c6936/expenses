// general setings
#define REC_FILE "/home/jon/Documents/Home/records.db"
#define GRAPH_DATA "./exp.dat"
#define CAT_OPTS "./categories.txt"
#define MAX_ROWS 4096 
#define MAX_STR 64
#define MAX_STR_CD 32

// for printing the database
#define CELL_WIDTH 16
#define CELL_SPACE 4
#define MAX_ROW_LEN 128

// prompts
#define PMT_MAIN "1.  Insert Record\n2.  Print database\n3.  Breakdown\n0.  Quit\n"
