/**
 * @file db.h
 * @author Jon Pina
 * @date 19 Dec 2021
 * @brief sqlite3 database interface
 *
 * Defines function prototypes that allow
 * c structs to be inserted (and retrieved) from an sqlite3 datase.
 */

#ifndef _DB_H_
#define _DB_H_

#include <stdlib.h>
#include "record.h"
#include "mylibs/lnls.h"

struct Breakdown
{
    lnls_node *catlistc;
    lnls_node *catlistd;
    lnls_node *amtlistc;
    lnls_node *amtlistd;
    float ctotal;
    float dtotal;
};

// sqlite3 Interface
int db_create();
lnls_node *db_read();
int db_read_callback(void *data, int argc, char **argv, char **colname);
int db_insert_rec(Record r);

//Data Output
void db_print(lnls_node *head);
struct Breakdown db_breakdown(lnls_node *head);
void db_breakdown_free(struct Breakdown bd);
void db_breakdown_stdio(struct Breakdown bd);
void db_breakdown_print(lnls_node *head);
void graph(lnls_node *list);

// Parse Data
lnls_node *db_get_cat_list(lnls_node *head);
float db_total(lnls_node *head);
float db_total_by_cat(lnls_node *head, char *cat);

// Filters
void filter_interactive(lnls_node *list);
void filter_category(lnls_node **list, char *category);
void filter_daterange(lnls_node **list, DateRange daterange, int inclusive);
void filter_description(lnls_node **list, char *description, int match_case);
void filter_type(lnls_node **list, char type);

// Sort
void db_row_swap(lnls_node **list, int m, int n);

// Misc
lnls_node *db_duplicate(lnls_node *head);
void db_free(lnls_node **head);
void str_toupper(char **str, size_t len);
char *trim_str_cd(char *str);

#endif
