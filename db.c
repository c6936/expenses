#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "record.h"
#include "dbg.h"
#include "config.h"
#include "db.h"
#include "colors.h"
#include "mylibs/date.h"
#include "mylibs/lnls.h"
#include "mylibs/reads.h"

int db_create()
{
	char *sql = "CREATE TABLE records(id INTEGER PRIMARY KEY, type TEXT, year INTEGER, month INTEGER, day INTEGER, category TEXT, description TEXT, amount REAL);";
	sqlite3 *db;
	char *sqlerr = NULL;

	int rc = sqlite3_open(REC_FILE, &db);
	check(rc == SQLITE_OK, "Unable to open database.");

	sqlite3_exec(db, sql, NULL, NULL, &sqlerr);
	check(rc == SQLITE_OK, "Could not execute SQL statement.");

	sqlite3_free(sqlerr);
	sqlite3_close(db);

	return 0;
error:
	return 1;
}

int gi;
lnls_node *grecs;

lnls_node *db_read()
{
    lnls_node *res = NULL;
    lnls_node *curr_node;
    lnls_node temp_node;
    Record r;
    sqlite3 *db;
    char *sqlerr = NULL;
    // open db
    int rc = sqlite3_open(REC_FILE, &db);
    check(rc == SQLITE_OK, "Unable to open database.")
    // initialize globals
    gi = 0;
    // perform read
    rc = sqlite3_exec(db, "SELECT * FROM records", db_read_callback, NULL, &sqlerr);
    check(rc == SQLITE_OK, "Unable to execute SQL.");
    // create new lnls to be returned
    for(; gi > 0; gi--)
    {
      curr_node = lnls_node_init();
      temp_node = lnls_get_nth(grecs, gi-1);
      memcpy(curr_node, &temp_node, sizeof(lnls_node));
      lnls_push(&res, &curr_node);
    }
    // cleanup
    lnls_list_free(&grecs);
    sqlite3_free(sqlerr);
    sqlite3_close(db);
    return res;
error:
    if(sqlerr)
    {
      printf("%s\n", sqlerr);
      sqlite3_free(sqlerr);
    }
    sqlite3_close(db);
    exit(1);
}

int db_read_callback(void *data, int argc, char **argv, char **colname)
{
  check(gi < MAX_ROWS, "gi > MAX_ROWS");
  // read data pulled from a row in the SQL table
  Record r;
  sscanf(argv[0], "%zu", &r.id);
  r.type = argv[1][0];
  r.date.year = atoi(argv[2]);
  r.date.month = atoi(argv[3]);
  r.date.day = atoi(argv[4]);
  strncpy(r.category, argv[5], MAX_STR_CD);
  r.category[strlen(argv[5])] = '\0';
  strncpy(r.description, argv[6], MAX_STR_CD);
  r.description[strlen(argv[6])] = '\0';
  r.amount = atof(argv[7]);
  // create a linked-list node
  lnls_node *n = lnls_node_init();
  n->vd = malloc(sizeof(Record));
  check(n->vd, "Memory error");
  memcpy(n->vd, &r, sizeof(Record));
  lnls_push(&grecs, &n);
  gi++;
  return 0;
error:
  exit(1);
}

int db_insert_rec(Record r)
{
	sqlite3 *db;
	int rc = sqlite3_open(REC_FILE, &db);
	check(rc == SQLITE_OK, "Unable to open database.");
	char *sqlerr = NULL;

	char sql[1024];
	snprintf(sql, 1024, "INSERT INTO records (type, year, month, day, category, description, amount) VALUES('%c', %d, %d, %d, '%s', '%s', %f);", r.type, r.date.year, r.date.month, r.date.day, r.category, r.description, r.amount);
	sql[1023] = '\0';

	rc = sqlite3_exec(db, sql, NULL, NULL, &sqlerr);
	check(rc == SQLITE_OK, "Could not execute SQL statement.");

	sqlite3_free(sqlerr);
	sqlite3_close(db);
	return 0;
error:
	if(sqlerr)
	{
		printf("SQL Error: %s\n", sqlerr);
		sqlite3_free(sqlerr);
	}
	sqlite3_close(db);
	return 1;
}

void db_print(lnls_node *head)
{
  //lnls_node *lhead = *head;
  printf("\n");
  //rec_list_print(lhead);
  rec_list_print(head);
  return;
}

struct Breakdown db_breakdown(lnls_node *head)
{
    struct Breakdown bd;
    lnls_node *clist = db_duplicate(head);
    lnls_node *dlist = db_duplicate(head);
    lnls_node *clist_head = clist;
    lnls_node *dlist_head = dlist;
    filter_type(&clist, 'C');
    filter_type(&dlist, 'D');
    float ctotal = 0.000;
    float dtotal = 0.000;
    lnls_node *cat_list_c = db_get_cat_list(clist);
    lnls_node *cat_list_head_c = cat_list_c;
    lnls_node *cat_list_d = db_get_cat_list(dlist);
    lnls_node *cat_list_head_d = cat_list_d;
    Record curr_rec;
    lnls_node *amt_list_c = NULL;
    lnls_node *amt_list_d = NULL;
    lnls_node *dummy = NULL;
    float curr_amt = 0.000;
    char *curr_cat;
    check(curr_cat = malloc(MAX_STR_CD), "memory error");

    while(cat_list_c)
    {
        dummy = lnls_node_init();
        memset(curr_cat, '\0', MAX_STR_CD);
        strncpy(curr_cat, cat_list_c->str, MAX_STR_CD);
        curr_amt = db_total_by_cat(clist, curr_cat);
        dummy->f = curr_amt;
        lnls_push(&amt_list_c, &dummy);
        cat_list_c = cat_list_c->next;
    }
    cat_list_c = cat_list_head_c;

   while(cat_list_d)
    {
        dummy = lnls_node_init();
        memset(curr_cat, '\0', MAX_STR_CD);
        strncpy(curr_cat, cat_list_d->str, MAX_STR_CD);
        curr_amt = db_total_by_cat(dlist, curr_cat);
        dummy->f = curr_amt;
        lnls_push(&amt_list_d, &dummy);
        cat_list_d = cat_list_d->next;
    }
    cat_list_d = cat_list_head_d;

    bd.catlistc = cat_list_c;
    bd.catlistd = cat_list_d;
    lnls_list_rev(&amt_list_c);
    bd.amtlistc = amt_list_c;
    lnls_list_rev(&amt_list_d);
    bd.amtlistd = amt_list_d;
    bd.ctotal = db_total(clist);
    bd.dtotal = db_total(dlist);

    db_free(&clist);
    db_free(&dlist);
    free(curr_cat);
    return bd;
error:
    exit(1);
}

void db_breakdown_free(struct Breakdown bd)
{
  lnls_node *ls = bd.catlistc;
  lnls_list_free(&ls);
  ls = bd.catlistd;
  lnls_list_free(&ls);
  ls = bd.amtlistc;
  lnls_list_free(&ls);
  ls = bd.amtlistd;
  lnls_list_free(&ls);
}

void db_breakdown_stdio(struct Breakdown bd)
{
  const size_t ROW_WIDTH = 32;
  char row[ROW_WIDTH+1];
  char *curr_cat = NULL;
  char curr_amt[MAX_STR_CD];
  int i;
  lnls_node *catlistc_save = (&bd)->catlistc;
  lnls_node *catlistd_save = (&bd)->catlistd;
  lnls_node *amtlistc_save = (&bd)->amtlistc;
  lnls_node *amtlistd_save = (&bd)->amtlistd;

  for(i = 0; i < ROW_WIDTH; i++) printf("-");
  printf(ANSI_COLOR_GREEN "\nCREDIT\n" ANSI_COLOR_RESET);
  for(i = 0; i < ROW_WIDTH; i++) printf("-");
  printf("\n");

  while(bd.catlistc)
  {
    memset(row, ' ', ROW_WIDTH);
    row[ROW_WIDTH] = '\0';
    curr_cat = bd.catlistc->str;
    memset(curr_amt, '\0', MAX_STR_CD);
    sprintf(curr_amt, "$%.2f", bd.amtlistc->f);
    // begin assembling row
    for(i = 0; (curr_cat[i] || curr_amt[i]) && i < MAX_STR_CD; i++)
    {
      if(i < strlen(curr_cat))
        row[i] = curr_cat[i];
      if(i < strlen(curr_amt))
        row[ROW_WIDTH-1-i] = curr_amt[strlen(curr_amt)-1-i];
    }
    printf("%s\n", row);
    bd.catlistc = bd.catlistc->next;
    bd.amtlistc = bd.amtlistc->next;
  }
  for(i = 0; i < ROW_WIDTH; i++) printf("-");
  printf("\nTotal:\t\t$%.2f", bd.ctotal);




  printf("\n\n\n");
  for(i = 0; i < ROW_WIDTH; i++) printf("_");
  printf("\n\n\n");



  for(i = 0; i < ROW_WIDTH; i++) printf("-");
  printf(ANSI_COLOR_RED "\nDEBIT\n" ANSI_COLOR_RESET);
  for(i = 0; i < ROW_WIDTH; i++) printf("-");
  printf("\n");

  while(bd.catlistd)
  {
    memset(row, ' ', ROW_WIDTH);
    row[ROW_WIDTH] = '\0';
    curr_cat = bd.catlistd->str;
    memset(curr_amt, '\0', MAX_STR_CD);
    sprintf(curr_amt, "$%.2f", bd.amtlistd->f);
    // begin assembling row
    for(i = 0; (curr_cat[i] || curr_amt[i]) && i < MAX_STR_CD; i++)
    {
      if(i < strlen(curr_cat))
        row[i] = curr_cat[i];
      if(i < strlen(curr_amt))
        row[ROW_WIDTH-1-i] = curr_amt[strlen(curr_amt)-1-i];
    }
    printf("%s\n", row);
    bd.catlistd = bd.catlistd->next;
    bd.amtlistd = bd.amtlistd->next;
  }
  for(i = 0; i < ROW_WIDTH; i++) printf("-");
  printf("\nTotal:\t\t$%.2f\n\n", bd.dtotal);

  bd.catlistc = catlistc_save;
  bd.amtlistc = amtlistc_save;
  bd.catlistd = catlistd_save;
  bd.amtlistd = amtlistd_save;
}

void db_breakdown_print(lnls_node *head)
{
  const size_t SPACE = 8;
  const size_t AMT_LEN = 16;
  const size_t TOTAL_WIDTH = MAX_STR_CD + SPACE + AMT_LEN + 1;
  char row[TOTAL_WIDTH + 1];
  char amt[AMT_LEN];
  size_t amt_len;
  size_t cat_len;
  int i, row_pos;
  int row_num = 0;

  for(i = 0; i <= TOTAL_WIDTH; i++)
      row[i] = '\0';

  char cat[MAX_STR_CD+1];
  float total;
  float totaltotal = 0.000;

  lnls_node *cat_list = db_get_cat_list(head);
  lnls_node *cat_list_head = cat_list;

  printf("\n");
  while(cat_list)
  {
    strncpy(cat, cat_list->str, MAX_STR_CD);
    total = db_total_by_cat(head, cat);
    totaltotal += total;
    cat_len = strlen(cat);
    row_pos = 0;

    for(i = 0; i < MAX_STR_CD; i++)
    {
      if(i < (int) cat_len)
        row[row_pos] = cat[i];
      else
      {
        if(row_num % 2 == 0)
          row[row_pos] = '-';
        else
          row[row_pos] = ' ';
      }
      row_pos++;
    }

    for(i = 0; i < SPACE; i++)
    {
      if(row_num % 2 == 0)
        row[row_pos] = '-';
      else
        row[row_pos] = ' ';
      row_pos++;
    }

    snprintf(amt, AMT_LEN, "$%.2f", total);
    amt_len = strlen(amt);
    for(i = 0; i < amt_len; i++)
    {
      if(i < amt_len)
        row[row_pos] = amt[i];
      else
        row[row_pos] = ' ';
      row_pos++;
    }
    row[TOTAL_WIDTH] = '\0';
    printf("%s\n", row);
    cat_list = cat_list->next;
    row_num++;
  }
  printf("\nTotal = $%.2f\n\n", totaltotal);
  lnls_list_free(&cat_list_head);
}

void graph(lnls_node *list)
{
  lnls_node *cat_list = db_get_cat_list(list);
  lnls_node *cat_list_save = cat_list;
  const char *data_source = GRAPH_DATA;
  FILE *fp = fopen(data_source, "w+");

loop: fprintf(fp, "%s %.2f", cat_list->str, cat_list->f);;
  cat_list = cat_list->next;
  if(cat_list)
  {
    fprintf(fp, "\n");
    goto loop;
  }
  lnls_list_free(&cat_list_save);
  fclose(fp);

  FILE *gnuplot = popen("gnuplot -persist", "w");
  if(!gnuplot)
  {
    perror("popen");
    exit(1);
  }

  fprintf(gnuplot, "set title \"Title\n");
  fflush(gnuplot);
  fprintf(gnuplot, "set grid\n");
  fflush(gnuplot);
  fprintf(gnuplot, "plot \"%s\" u (column(0)):2:xtic(1) with boxes\n", data_source);
  fflush(gnuplot);
  pclose(gnuplot);
}

lnls_node *db_get_cat_list(lnls_node *head)
{
  lnls_node *head_save = head;
  lnls_node *cat_list = NULL;
  lnls_node *dummy = NULL;
  Record r;

  while(head)
  {
    memcpy(&r, head->vd, sizeof(Record));
    if(r.type != 'C' && r.type != 'D')
      goto skip;

    dummy = lnls_node_init();
    strncpy(dummy->str, r.category, MAX_STR_CD);

    if(!lnls_list_contains(cat_list, 4, dummy))
      lnls_push(&cat_list, &dummy);
    else
      lnls_list_free(&dummy);

skip:
    head = head->next;
  }

  dummy = cat_list;
  head = head_save;
  while(dummy)
  {
    dummy->f = db_total_by_cat(head, dummy->str);
    dummy = dummy->next;
  }

  return cat_list;
}

float db_total(lnls_node *head)
{
    Record curr_rec;
    float res = 0.000;
    while(head)
    {
        memcpy(&curr_rec, head->vd, sizeof(Record));
        if(curr_rec.type == 'C' || curr_rec.type == 'D')
            res += curr_rec.amount;
        head = head->next;
    }
    return res;
}

float db_total_by_cat(lnls_node *head, char *cat)
{
  Record curr_rec;
  float res = 0;
  while(head)
  {
    memcpy(&curr_rec, head->vd, sizeof(Record));
    if(curr_rec.type == 'C' || curr_rec.type == 'D')
    {
      if(strcmp(curr_rec.category, cat) == 0)
        res += curr_rec.amount;
    }
    head = head->next;
  }
  return res;
}

DateRange daterange_input_interactive();
void filter_interactive(lnls_node *list)
{
  int choice;
  char c;
  char *str;
  DateRange dr;
pmt_filters:
  printf("\nSelect Filter\n----------------\n");
  printf("1. Category\n2. Date Range\n3. Description\n4. Type\n0. Done\n");
  choice = read_int("> ");
  switch(choice)
  {
  case 1:
    str = read_string("Category> ");
    filter_category(&list, str);
    free(str);
    goto pmt_filters;
  case 2:
    dr = daterange_input_interactive();
    filter_daterange(&list, dr, 1);
    goto pmt_filters;
  case 3:
    str = read_string("Description> ");
    filter_description(&list, str, 0);
    free(str);
    goto pmt_filters;
case 4:
    c = read_char("Type> ");
    filter_type(&list, c);
    goto pmt_filters;
  case 0:
  }
}

void filter_category(lnls_node **list, char *category)
{
    lnls_node *curr_node = *list;
    lnls_node *next_node = NULL;
    Record r;
    int rc;
    while(curr_node)
    {
        next_node = curr_node->next;
        memcpy(&r, curr_node->vd, sizeof(Record));

        // main logic
        if(r.type == 'C' || r.type == 'D')
        {
            // strcmp returns 0 on =
            rc = strcmp(r.category, category);
            if(rc != 0)
              r.type = 'X';
        }
        // At this point, the record will either be the same as it was, or marked 'X'.
        // Copy the data back to the stack.
        memcpy(curr_node->vd, &r, sizeof(Record));

        curr_node = next_node;
        next_node = NULL;
    }
    goto done;
error:
        exit(1);
done:;
}

void filter_daterange(lnls_node **list, DateRange daterange, int inclusive)
{
    lnls_node *curr_node = *list;
    lnls_node *next_node = NULL;
    Record r;
    int rc;
    while(curr_node)
    {
        next_node = curr_node->next;
        memcpy(&r, curr_node->vd, sizeof(Record));

        // main logic
        if(r.type == 'C' || r.type == 'D')
        {
            rc = date_is_in_range(r.date, daterange, inclusive);
            switch(rc)
            {
            case 0:
                r.type = 'X';
                break;
            case -1:
                sentinel("DateRange error");
                break;
            default:;
            }
        }
        // At this point, the record will either be the same as it was, or marked 'X'.
        // Copy the data back to the stack.
        memcpy(curr_node->vd, &r, sizeof(Record));

        curr_node = next_node;
        next_node = NULL;
    }
    goto done;
error:
        exit(1);
done:;
}

// description must not be const
void filter_description(lnls_node **list, char *description, int match_case)
{
  lnls_node *curr_node = *list;
  lnls_node *next_node = NULL;
  Record r;
  int rc;
  char *desc_given = malloc(MAX_STR_CD);
  char *desc_from_rec = malloc(MAX_STR_CD);

  while(curr_node)
  {
    next_node = curr_node->next;
    memcpy(&r, curr_node->vd, sizeof(Record));


    if(r.type == 'C' || r.type == 'D')
    {
      strncpy(desc_given, description, MAX_STR_CD);
      strncpy(desc_from_rec, r.description, MAX_STR_CD);

      if(!match_case)
      {
        str_toupper(&desc_given, MAX_STR_CD);
        str_toupper(&desc_from_rec, MAX_STR_CD);
      }

      if(strcmp(desc_given, desc_from_rec) != 0)
      {
        r.type = 'X';
        memcpy(curr_node->vd, &r, sizeof(Record));
      }
    }
    curr_node = next_node;
    next_node = NULL;
  }
  free(desc_given);
  free(desc_from_rec);
}

void filter_type(lnls_node **list, char type)
{
    lnls_node *curr_node = *list;
    lnls_node *next_node = NULL;
    Record r;
    while(curr_node)
    {
        next_node = curr_node->next;
        memcpy(&r, curr_node->vd, sizeof(Record));
        if(r.type != type)
        {
            r.type = 'X';
            memcpy(curr_node->vd, &r, sizeof(Record));
        }
        curr_node = next_node;
        next_node = NULL;
    }
}

void db_row_swap(lnls_node **list, int m, int n)
{
    check((m >= 0) && (n >= 0), "Parameter error");
    check((m < MAX_ROWS) && (n < MAX_ROWS), "Parameter error");
    lnls_node *llist = *list;
    lnls_node tmp_node;

    memcpy(&tmp_node, &llist[m], sizeof(lnls_node));
    memcpy(&llist[m], &llist[n], sizeof(lnls_node));
    memcpy(&llist[n], &tmp_node, sizeof(lnls_node));
    return;
error:
    db_free(&llist);
    exit(1);
}

lnls_node *db_duplicate(lnls_node *head)
{
    if(!head)   return NULL;
    lnls_node *result = lnls_node_init();
    result->vd = malloc(sizeof(Record));
    memcpy(result->vd, head->vd, sizeof(Record));
    result->next = db_duplicate(head->next);
    return result;
}

void db_free(lnls_node **head)
{
  lnls_node *next_node;
  while(*head)
  {
    next_node = (*head)->next;
    // this line is the only difference between the stock free function
    if((*head)->vd)   free((*head)->vd);
    (*head)->vd = NULL;
    if(*head)   free(*head);
    *head = NULL;
    *head = next_node;
    }
}

void str_toupper(char **str, size_t len)
{
  int i;
  char *s = *str;
  for(i = 0; s[i] != '\0' && i < len; i++)
  {
    if(s[i] >= 'a' && s[i] <= 'z')
      s[i] = s[i] - 32;
  }
}

char *trim_str_cd(char *str)
{
  char *res = malloc(MAX_STR_CD);
  check(res, "Memory error");
  int i;
  size_t orig_len = strlen(str);

  for(i = 0; i < MAX_STR_CD; i++)
    res[i] = '\0';

  for(i = 0; i < MAX_STR_CD; i++)
  {
    if(i < orig_len)
      res[i] = str[i];
    else
      break;
  }
  return res;
error:
  //if(res) free(&res);
  exit(1);
}
