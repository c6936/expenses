#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dbg.h"
#include "record.h"
#include "db.h"
#include "colors.h"
#include "config.h"
#include "mylibs/lnls.h"
#include "mylibs/date.h"
#include "mylibs/reads.h"

Record rec_blank()
{
	Record blank;
	int i;

      blank.id = 0;
	blank.type = 'X';
	blank.date.year = 0;
	blank.date.month = 0;
	blank.date.day = 0;
	for(i = 0; i < MAX_STR_CD; i++)
	{
		blank.category[i] = '\0';
		blank.description[i] = '\0';
	}
	blank.amount = 0.00000;
	return blank;
}

char *rec_to_string(Record r)
{
  char tmp[1024];
  snprintf(tmp, 1023,
           "id = %zu\n"
           "Type = %c\n"
           "Year = %d\n"
           "Month = %d\n"
           "Day = %d\n"
           "Category = %s\n"
           "Description = %s\n"
           "Amount = %f"
            ,r.id, r.type, r.date.year, r.date.month, r.date.day, r.category, r.description, r.amount);
  tmp[1023] = '\0';

char *res = malloc(sizeof(tmp));
    check(res, "Memory error");
    strncpy(res, tmp, 1024);
    return res;
error:
    return NULL;
}

void rec_print_header()
{
  check(CELL_WIDTH >= 12, "CELLWIDTH !>= 10\n");
  check(MAX_ROW_LEN > (CELL_WIDTH + CELL_SPACE), "Check globals.h")

  char res[MAX_ROW_LEN+1];
  int i, j, k;
  const size_t NUM_FIELDS = 6;
  char curr_field[CELL_WIDTH+1];
  size_t curr_cell_width;
  size_t curr_cell_space;

  for(j = 0; j < MAX_ROW_LEN; j++)
    res[j] = '\0';
  k = 0;

  for(i = 0; i < NUM_FIELDS; i++)
  {
    // set default cell width and space
    // set per-field in the cases below
    curr_cell_width = CELL_WIDTH+1;
    curr_cell_space = CELL_SPACE;
    for(j = 0; j <= CELL_WIDTH; j++)
      curr_field[j] = '\0';

    switch(i)
    {
    case 0:
      curr_cell_width = 7;
      curr_cell_space =  4;
      strncpy(curr_field, "Index", curr_cell_width);
      break;
    case 1:
      curr_cell_width = 10; // because mm/dd/yyyy
      curr_cell_space = 4;
      strncpy(curr_field, "Date", curr_cell_width);
      break;
    case 2:
      curr_cell_width = 3;
      curr_cell_space = 4;
      strncpy(curr_field, "C/D", curr_cell_width);
      break;
    case 3:
      strncpy(curr_field, "Category", curr_cell_width);
      break;
    case 4:
      strncpy(curr_field, "Description", curr_cell_width);
      break;
    case 5:
      strncpy(curr_field, "Amount", curr_cell_width);
      break;
    }
    for(j = 0; j < curr_cell_width; j++)
    {
      if((j < curr_cell_width) && (j < strlen(curr_field)) && (k < MAX_ROW_LEN))
        res[k] = curr_field[j];
      else
        res[k] = ' ';
      k++;
    }
    for(j = 0; j < curr_cell_space; j++)
    {
      res[k] = ' ';
      k++;
      }
  }
  res[MAX_ROW_LEN] = '\0';
  printf("%s\n", res);
  return;
error:
  exit(1);
}

void rec_print_row(Record *r, const int row_num)
{
  check(CELL_WIDTH >= 12, "CELLWIDTH !>= 10\n");
  check(MAX_ROW_LEN > (CELL_WIDTH + CELL_SPACE), "Check globals.h")

  char res[MAX_ROW_LEN+1];
  int i, j, k;
  const size_t NUM_FIELDS = 6;
  char curr_field[CELL_WIDTH+1];
  size_t curr_cell_width;
  size_t curr_cell_space;

  // placeholder string needed for stack/heap bs
  char *tmp_str = NULL;

  for(j = 0; j < MAX_ROW_LEN; j++)
    res[j] = '\0';
  k = 0;

  for(i = 0; i < NUM_FIELDS; i++)
  {
    // set default cell width and space
    curr_cell_width = CELL_WIDTH+1;
    curr_cell_space = CELL_SPACE;
    for(j = 0; j <= CELL_WIDTH; j++)
      curr_field[j] = '\0';

    switch(i)
    {
    case 0:
      curr_cell_width = 7;
      curr_cell_space = 4;
      snprintf(curr_field, MAX_STR_CD, "%d", r->id);
      break;
    case 1: // date
      curr_cell_width = 10; // because mm/dd/yyyy
      curr_cell_space = 4;
      tmp_str = date_to_string(r->date, 0);
      strncpy(curr_field, tmp_str, curr_cell_width);
      free(tmp_str);
      break;
    case 2: // C/D
      curr_cell_width = 3;
      curr_cell_space = 4;
      curr_field[0] = r->type;
      curr_field[1] = '\0';
      break;
    case 3: // category
      strncpy(curr_field, r->category, curr_cell_width);
      break;
    case 4: // description
      strncpy(curr_field, r->description, curr_cell_width);
      break;
    case 5: // amount
      snprintf(curr_field, curr_cell_width, "$%.2f", r->amount);
      break;
    }
    for(j = 0; j < curr_cell_width; j++)
    {
      if((j < curr_cell_width) && (j < strlen(curr_field)) && (k < MAX_ROW_LEN))
        res[k] = curr_field[j];
      else
        res[k] = ' ';
      k++;
    }
    for(j = 0; j < curr_cell_space; j++)
    {
      res[k] = ' ';
      k++;
    }
  }
  res[MAX_ROW_LEN] = '\0';
  printf("%s\n", res);
  return;
error:
  exit(1);
}

void rec_list_print(lnls_node *list)
{
    Record r;
    const size_t HEAD_REPEAT = 20; // print the header every HEAD_REPEAT rows
    size_t head_ct = HEAD_REPEAT;
    float c_total = 0, d_total = 0, total = 0;
    int i = 0;

    while(list)
    {
        memcpy(&r, list->vd, sizeof(Record));
        if(r.type == 'C' || r.type == 'D')
        {
          // update totals
          if(r.type == 'C')
            c_total += r.amount;
          else if(r.type == 'D')
            d_total += r.amount;

          // print header if needed
          if(head_ct >= HEAD_REPEAT)
          {
            rec_print_header();
            head_ct = 0;
          }
          head_ct++;
          // rec_print_row(&r, i++);
          rec_print_row(&r, r.id);
        }
        list = list->next;
    }
    total = c_total - d_total;
    printf(ANSI_COLOR_GREEN "\nCredit  =  $%.2f\n" ANSI_COLOR_RESET, c_total);
    printf(ANSI_COLOR_RED     "Debit   =  $%.2f\n" ANSI_COLOR_RESET, d_total);
    if(total < 0)
      printf(ANSI_COLOR_RED   "Total   =  $%.2f\n" ANSI_COLOR_RESET, total);
    else if(total > 0)
      printf(ANSI_COLOR_GREEN "Total   =  $%.2f\n" ANSI_COLOR_RESET, total);
    else
      printf(                 "Total   =  Broke Even\n");
}

Record rec_input()
{
	char *buff;
	Record res;
	res.type = read_char("Type> ");
	res.date = date_input();

	buff = read_string("Category> ");
	if(strlen(buff) >= MAX_STR_CD)
		buff[MAX_STR_CD-1] = '\0';
	strncpy(res.category, buff, MAX_STR_CD);
	free(buff);

	buff = read_string("Description> ");
	if(strlen(buff) >= MAX_STR_CD)
		buff[MAX_STR_CD-1] = '\0';
	strncpy(res.description, buff, MAX_STR_CD);
	free(buff);

	res.amount = read_float("Amount> ");
	return res;
}
