CC=gcc
CFLAGS=-Wall -Wextra -g
SRC=db.c record.c main.c
OBJ=$(SRC:.c=.o)
EXEC=exp
LIBS=-lsqlite3 ./mylibs/*.o

default: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) $(CFLAGS) -o $(EXEC) $(OBJ) $(LIBS)

main.o: main.c db.o record.o
	$(CC) -c -g main.c
db.o: db.c db.h record.o
	$(CC) -c -g db.c
record.o: record.c record.h db.o
	$(CC) -c -g record.c

.PHONY: clean rebuild

rebuild:
	make clean
	clear
	make

clean:
	rm -f $(EXEC)
	rm -f $(OBJ)
	rm -f vgcore*
	cd mylibs/
	rm -f *.o

