#include "reads.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dbg.h"

char *read_string(const char *prompt)
{
	char buff[MAX_STR_READS] = {'\0'};
	char *str = NULL;

	// Show prompt
	if(prompt) printf("%s", prompt);

	// Scanset - read while curr char != newline
	// %*c means read until newline, then discard it.
	scanf("%[^\n]%*c", buff);
	fflush(stdin);
	buff[strlen(buff)] = '\0';

	// Allocate heap
	str = malloc(strlen(buff)+1);
	check(str, "Memory error");

	// Move to string to final array
	strcpy(str, buff);
	
	return str;
error:
	if(str) free(str);
	exit(1);
}

int read_int(const char *prompt)
{
	char *buff = read_string(prompt);
	int res = atoi(buff);
	if(buff)	free(buff);
	return res;
}

char read_char(const char *prompt)
{
	char *buff = read_string(prompt);
	char res = buff[0];
	if(buff)	free(buff);
	return res;
}

float read_float(const char *prompt)
{
	char *buff = read_string(prompt);
	float res = atof(buff);
	if(buff)	free(buff);
	return res;
}
