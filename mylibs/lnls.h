/**
 * @file lnls.h
 * @author Jon Pina
 * @date 14 January 2022
 * @brief Function headers for a multi-type linked-lists.
 */

#ifndef LNLS_H
#define LNLS_H

#include <stdlib.h>

#define MAX_STR_LNLS 1024

typedef struct lnls_node lnls_node;
///@brief Struct containing several data types and a pointer to some other struct of the same type.
struct lnls_node
{
	lnls_node *next;
	int i;
	size_t st;
	float f;
	char c;
	char str[MAX_STR_LNLS];
	void *vd;
};

lnls_node *lnls_node_init();
void lnls_list_free(lnls_node **head);

void lnls_push(lnls_node **head, lnls_node **data);
lnls_node lnls_pop(lnls_node **head);

void lnls_node_print(lnls_node *node);
void lnls_list_print(lnls_node *head);
size_t lnls_list_len(lnls_node *head);
int lnls_list_contains(lnls_node *head, int memnum, lnls_node *data);
lnls_node lnls_get_nth(lnls_node *head, size_t n);
void lnls_node_swap(lnls_node **head, size_t a, size_t b);
void lnls_list_rev(lnls_node **head);

#endif
