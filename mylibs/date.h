/**
 * @file date.h
 * @author Jon Pina
 * @date 19 Dec 2021
 * @brief Provide a very simple interface for dates and range of dates.
 * Provides two objects: Date and DateRange.  Dates consist of 3 integers - year, month, & day.  DateRange consists of 2 Dates, ideally in chronilogical order, though this isn't necessary.
 */

#ifndef _DATES_H_
#define _DATES_H_

struct Date
{
	int year;
	int month;
	int day;
};
typedef struct Date Date;

struct DateRange
{
	Date low;
	Date hi;
};
typedef struct DateRange DateRange;

Date get_curr_date();
char *date_to_string(Date date, int style);
Date date_input();
DateRange daterange_input();
DateRange daterange_input_interactive();
int date_eq(Date d1, Date d2);
int date_lt(Date d1, Date d2);
int date_lte(Date d1, Date d2);
int date_gt(Date d1, Date d2);
int date_gte(Date d1, Date d2);
int date_is_in_range(Date date, DateRange daterange, int inclusive);

#endif
