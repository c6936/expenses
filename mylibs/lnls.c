/**
 * @file lnls.c
 * @date 14 January 2022
 * @brief Function implementations for prototypes for found in lnls.h.
 *
 * Normally, a struct at the heart of a linked list contains two members - data of some
 * type and a poniter to another struct.  My implementation uses several data types and
 * a pointer to act as a general-purpose linked-list.  This can be useful because I don't
 * need to implement several single data-type lists, and I can shove more data in
 * each node.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lnls.h"
#include "dbg.h"

/**
 * @brief Create a new node on the heap.
 * @return struct lnls_node allocated on the heap.
 */
lnls_node *lnls_node_init()
{
	lnls_node *n = malloc(sizeof(lnls_node));
	check(n, "Memrory error");

	n->next = NULL;
	n->i = 0;
	n->st = 0;
	n->f = 0.0;
	n->c = '\0';
	n->str[0] = '\0';
	n->vd = NULL;
	//
	int i;
	for(i = 0; i < MAX_STR_LNLS; i++)
		n->str[i] = '\0';
	return n;
error:
	if(n)	free(n);
	exit(1);
}

/**
 * @brief Free and entire linked-list.
 * @param head The first node in the list.
 */
void lnls_list_free(lnls_node **head)
{
	lnls_node *next_node;
	while(*head)
	{
		next_node = (*head)->next;
		if(*head)	free(*head);
		*head = NULL;
		*head = next_node;
	}
}

/**
 * @brief Push a node onto the list.  LIFO
 * @param head The first node in the list.
 * @param data The heap-allocated node to be pushed onto the list.
 * @note The data struct must already be on the heap.
 */
void lnls_push(lnls_node **head, lnls_node **data)
{
	(*data)->next = *head;
	*head = *data;
}

/**
 * @brief Pop the first node off the list.
 * @param head The first node in the list.
 * @return A stack-allocated node.
 */
lnls_node lnls_pop(lnls_node **head)
{
//	check(*head, "Gimme head!");
	lnls_node res;
	if(!*head)	return res;
	memcpy(&res, *head, sizeof(lnls_node));

	lnls_node *old_head = *head;
	*head = (*head)->next;
	free(old_head);
	old_head = NULL;
	return res;
error:
	// TODO: Free shit
	exit(1);
}

/**
 * @brief Dump node members to stdin.
 * @param node A pointer to some lnls_node.
 */
void lnls_node_print(lnls_node *node)
{
	printf("integer = %d\n", node->i);
	printf("size_t = %zu\n", node->st);
	printf("float = %f\n", node->f);
	printf("char = %c\n", node->c);
	printf("string = %s\n", node->str);
}

/**
 * @brief Call lnls_node_print for each node in the linked-list.
 * @param head The first node in the list.
 */
void lnls_list_print(lnls_node *head)
{
	while(head)
	{
		lnls_node_print(head);
		printf("---------------------------\n");
		head = head->next;
	}
}

/**
 * @brief Get the number of nodes in the list.
 * @param head The first node in the list.
 * @return The number of nodes in the list.
 */
size_t lnls_list_len(lnls_node *head)
{
	size_t i;
	for(i = 0; head; i++)
		head = head->next;
	return i;
}

/**
  * @brief Compare members of an lnls_node.
  * @param head The first node in the list.
  * @param memnum Which struct member to compare.  See lnls.h for member order.
  * @param data A struct to compare head[n] to.
  * @return 1 for yes, 0 for no
  */
/*
 * Member Numbers:
 * 0 = int i
 * 1 = size_t st
 * 2 = float f
 * 3 = char c
 * 4 = char *str
*/
int lnls_list_contains(lnls_node *head, int memnum, lnls_node *data)
{
	lnls_node curr_node;
	int i;

	// a = data
	// b = curr_node
	for(i = 0; head; i++, head = head->next)
	{
		switch(memnum)
		{
		case 0:
			if(data->i == head->i)
				return 1;
			else
				break;
		case 1:
			if(data->st == head->st)
				return 1;
			else
				break;
		case 2:
			if(data->f == head->f)
				return 1;
			else
				break;
		case 3:
			if(data->c == head->c)
				return 1;
			else
				break;
		case 4:
			if(strcmp(data->str, head->str) == 0)
				return 1;
			else
				break;
		}
	}
	return 0;
}

/**
 * @brief Return the nth (0 indexed) node.
 * @param head The first node in the list.
 * @param n The struct to get.
 * @return A node. */
lnls_node lnls_get_nth(lnls_node *head, size_t n)
{
	lnls_node res;
	size_t i;
	size_t list_len = lnls_list_len(head);

	check(list_len > n, "list len = %zu | n = %zu", list_len, n);

	for(i = 0; i < n; i++)
		head = head->next;
	memcpy(&res, head, sizeof(lnls_node));
error:
	return res;
}
/*
void lnls_node_swap(lnls_node **head, size_t a, size_t b)
{
	lnls_node *lhead = *head;
	lnls_node node_a, node_b, temp_next;
	size_t len = lnls_list_len(*head);
	size_t i;

	check(a < len && b < len, "Out of bounds.");
	if(a == b) goto error;
	if(a > b)
	{
		size_t c = b;
		b = a;
		a = c;
	}

	node_a = lnls_get_nth(lhead, a);
	node_b = lnls_get_nth(lhead, b);

	for(i = 0; i < a; i++)
		lhead = lhead->next;
	// what to do with lnls_node->next during the swap?
	temp_next = lhead->next;
	memcpy(lhead, node_b, sizeof(lnls_node));
	memcpy(lhead->next, sizeof(lnls_node));

error:
	return;
}
*/

void lnls_list_rev(lnls_node **head)
{
  lnls_node *current = *head;
  lnls_node *next = NULL;
  lnls_node *prev = NULL;
  
  while(current)
  {
    next = current->next;
    current->next = prev;
    prev = current;
    current = next;
  }
  *head = prev;
}

/*
int main()
{
	lnls_node *list = NULL;

	lnls_node *n1 = lnls_node_init();
	lnls_node *n2 = lnls_node_init();
	lnls_node *n3 = lnls_node_init();
	lnls_node *tocmp = lnls_node_init();

	n1->i = 1;
	n2->i = 2;
	n3->i = 3;
	strncpy(n1->str, "test1", 6);
	strncpy(n2->str, "test2", 6);
	strncpy(n3->str, "test3", 6);

	lnls_push(&list, &n1);
	lnls_push(&list, &n2);
	lnls_push(&list, &n3);
	tocmp->i = 4;
	strncpy(tocmp->str, "test4", 6);

	printf("%d\n", lnls_list_contains(list, 4, tocmp));

	lnls_list_free(&list);
	lnls_list_free(&tocmp);
	return 0;
}
*/
