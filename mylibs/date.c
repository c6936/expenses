/**
 * @file date.c
 * @author Jon Pina
 * @date 19 Dec 2021
 * @brief Function implementations for prototypes found in date.h.
 *
 * Provides a very basic date object.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "dbg.h"
#include "date.h"
#include "reads.h"

/**
 * @brief Get current date.
 *
 * Create a new date object in the stack containing the current year, month, and date.
 */
Date get_curr_date()
{
	Date cdate;
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	cdate.year  = tm.tm_year + 1900;
	cdate.month = tm.tm_mon + 1;
	cdate.day   = tm.tm_mday;

	return cdate;
}

/**
 * @brief Convert a Date to a string of a given format.
 *
 * Given a Date struct, return a string of a specified format e.g. mm/dd/yyyy or yyyy-mm-dd.
 * @param date The Date struct to be converted to a string.
 * @param style 0 = m/d/yyyy
 * @param style 1 = yyyy-m-d
 * @return A pointer to heap mempory containing the specified output string.
 * @waring malloc()
 */
char *date_to_string(Date date, int style)
{
	char tmp[16];
	char *res;

	switch(style)
	{
	// m/d/yyyy
	case 0:
		snprintf(tmp, 11, "%d/%d/%d", date.month, date.day, date.year);
		break;
	// y-m-d
	case 1:
		snprintf(tmp, 11, "%d-%d-%d", date.year, date.month, date.day);
		break;
	default:
		snprintf(tmp, 5, "error");
	}

	res = malloc(strlen(tmp) + 1);
	check(res, "Memory error");

	strncpy(res, tmp, strlen(tmp) + 1);
	res[strlen(res)] = '\0';
error:
	return res;
}

/**
 * @brief Accept input form stdin.
 *
 * Read 3 integers from stdin and place them in this.year, this.mont, & this.day, respectively.
 * @note Invalid input will result in a value of 0.
 */
Date date_input()
{
	Date res;

	res.year = read_int("Year> ");
	res.month = read_int("Month> ");
	res.day = read_int("Day> ");

	return res;
}

/**
 * @brief Read two (different) dates.
 *
 * Read the first date, then a second date.  Ideally, the first date will be before or equal to the second, but this is not necessary.
 */
DateRange daterange_input()
{
	DateRange dr;
	Date low, hi;

	printf("LOW date input.\n");
	low = date_input();
	printf("HI date input.\n");
	hi = date_input();

	dr.low = low;
	dr.hi = hi;
	return dr;
}

DateRange daterange_input_interactive()
{
	DateRange res;
	Date d;
	int choice;

pmt_dr_in_int:
	printf("1.  Current Month\n2.  Previous Month\n3.  Custom\n4.  Max\n> ");
	choice = read_int(NULL);

	switch(choice)
	{
	case 1:
		d = get_curr_date();
		d.day = 1;
		res.low = d;
		d.day = 31;
		res.hi = d;
		break;
	case 2:
		d = get_curr_date();
		d.day = 1;
		if(d.month > 1)
		{
			d.month--;
			res.low = d;
			d.day = 31;
			res.hi = d;
		}
		else if(d.month == 1)
		{
			d.year--;
			d.month = 12;
			res.low = d;
			d.day = 31;
			res.hi = d;
		}
		break;
	case 3:
		res = daterange_input();
		break;
	case 4:
		d.year = -32000;
		d.month = -32000;
		d.day = -32000;
		res.low = d;
		d.year = 32000;
		d.month = 32000;
		d.day = 32000;
		res.hi = d;
		break;
	default:
			goto pmt_dr_in_int;
	}
	return res;
}

/**
 * @brief Is d1 = d2?
 * @return 1 True
 * @return 0 False
 */
int date_eq(Date d1, Date d2)
{
	if(d1.year  == d2.year  &&
	   d1.month == d2.month &&
	   d1.day   == d2.day    )
	return 1;
	else
		return 0;
}

/*
 * @brief Is d1 < d2?
 * @return 1 True
 * @return 0 False
 */
int date_lt(Date d1, Date d2)
{
	if(d1.year  >  d2.year)	  return 0;
	if(d1.year  <  d2.year)	  return 1;
	if(d1.month >  d2.month)  return 0;
	if(d1.month <  d2.month)  return 1;
	if(d1.day   >  d2.day)	  return 0;
	if(d1.day   == d2.day)	  return 0;
	else			  return 1;
}

/**
 * @brief Is d1 <= d2?
 * @return 1 True
 * @return 0 False
 */
int date_lte(Date d1, Date d2)
{
	if(date_eq(d1, d2) || date_lt(d1, d2))
		return 1;
	else
		return 0;
}

/**
 * @brief Is d1 > d2?
 * @return 1 True
 * @return 0 False
 */
int date_gt(Date d1, Date d2)
{
	if(d1.year  <  d2.year)	 return 0;
	if(d1.year  >  d2.year ) return 1;
	if(d1.month <  d2.month) return 0;
	if(d1.month >  d2.month) return 1;
	if(d1.day   <  d2.day)	 return 0;
	if(d1.day   == d2.day)	 return 0;
	else			 return 1;
}

/**
 * @brief Is d1 >= d2?
 * @return 1 True
 * @return 0 False
 */
int date_gte(Date d1, Date d2)
{
	if(date_eq(d1, d2) || date_gt(d1, d2))
		return 1;
	else
		return 0;	
}

/**
 * @brief Is the Date within the DateRange?
 * @param date The date in question.
 * @param daterange The DateRange in which the Date should be in between.
 * @param inclusive 1 = inclusive
 * @param inclusive 0 = exclusive
 * @return 1 = Date is within DateRange
 * @return 0 = Date is not within DateRange
 * @return -1 = Error - Invalid value for @param inclusive.
 */
int date_is_in_range(Date date, DateRange daterange, int inclusive)
{
	switch(inclusive)
	{
	case 1:
		if(date_gte(date, daterange.low) && date_lte(date, daterange.hi))
			return 1;
		else
			return 0;
	case 0:
		if(date_gt(date, daterange.low) && date_lt(date, daterange.hi))
			return 1;
		else
			return 0;
	default:
		return -1;
	}
}
