#ifndef LINE_IO_H
#define LINE_IO_H

#include <stdio.h>
#include <stdlib.h>

#define MAX_STR_READS 512

char *read_string(const char *prompt);
int read_int(const char *prompt);
char read_char(const char *prompt);
float read_float(const char *prompt);

#endif
