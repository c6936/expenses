#include <stdio.h>
#include <stdlib.h>

#include "dbg.h"
#include "config.h"
#include "record.h"
#include "db.h"
#include "mylibs/date.h"
#include "mylibs/reads.h"
#include "mylibs/lnls.h"

#include <sqlite3.h>

void put_stuff()
{
  Record r;
  r.type = 'C';
  r.date = get_curr_date();
  strcpy(r.category, "TestCat0");
  strcpy(r.description, "TestDesc0");
  r.amount = 0.0;
  db_insert_rec(r);

  r.date.year = 2021;
  r.date.month = 1;
  r.date.day = 1;
  strcpy(r.category, "TestCat1");
  strcpy(r.description, "TestDesc1");
  r.amount = 1.1;
  db_insert_rec(r);

  r.date.year = 2023;
  r.date.month = 5;
  r.date.day = 31;
  strcpy(r.category, "TestCat2");
  strcpy(r.description, "TestDesc2");
  r.amount = 2.2;
  db_insert_rec(r);
}

// IDK why this declartion needs to be here, but it does.
DateRange daterange_input_interactive();
// void db_breakdown_test();
void test()
{
}

int main(int argc, char *argv[])
{
    // test();
    // return 0;

    lnls_node *list = NULL;
    Record rec;
    int choice;
    char to_graph = 'n';

pmt_main:
    printf("%s> ", PMT_MAIN);
    choice = read_int(NULL);
    switch(choice)
    {
    case 0:
        break;
    case 1:
        // add record
        rec = rec_input();
        db_insert_rec(rec);
        goto pmt_main;
    case 2:
        // print database
        list = db_read();
        filter_interactive(list);
        db_print(list);
        to_graph = read_char("Graph? (y/N)> ");
        if(to_graph == 'y' || to_graph == 'Y')
          graph(list);
        db_free(&list);
        list = NULL;
        goto pmt_main;
    case 3:
      // breakdown
      list = db_read();
      filter_daterange(&list, daterange_input_interactive(), 1);
      struct Breakdown bd = db_breakdown(list);
      db_breakdown_stdio(bd);
      db_breakdown_free(bd);
      to_graph = read_char("Graph? (y/N)> ");
      if(to_graph == 'y' || to_graph == 'Y')
        graph(list);
      db_free(&list);
      list = NULL;
    default:
        goto pmt_main;
    }
    return 0;
}
